﻿using System;

namespace AustralianCats
{
    public class AccountModel
    {
        public string Token { get; set; }
        public int? CatID { get; set; }
        public string UserName { get; set; }
    }
}


