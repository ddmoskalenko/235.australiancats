﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;


namespace AustralianCats
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// for future extetsion
        /// </summary>
        [Display(Name = "Favorite cat ID")]
        public int FavoritetCatID { get; set; }
    }
}
