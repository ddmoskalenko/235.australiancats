using System.Threading;
using System.Threading.Tasks;

namespace AustralianCats.DataAccessRemote
{
    /// <summary>
    /// Предоставляет методы для обращения к сторонним сервисам через REST API
    /// </summary>
    public interface IRemoteRestApi
    {

#region GET

        /// <summary>
        /// Выполняет синхронный запрос GET
        /// </summary>
        TResult Get<TResult>(string relativeUri, CancellationToken cancellationToken = default);

        /// <summary>
        /// Выполняет aсинхронный запрос GET
        /// </summary>
        Task<TResult> GetAsync<TResult>(string relativeUri, CancellationToken cancellationToken = default);

#endregion
    }
}