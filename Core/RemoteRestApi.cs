﻿using Microsoft.AspNetCore.Http;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;

namespace AustralianCats.DataAccessRemote
{
    internal sealed class RemoteRestApi : IRemoteRestApi
    {
        private delegate Task<TResult> ResponseDeserializer<TResult>(HttpResponseMessage response);

        private readonly Random _random = new Random();

        private readonly HttpClient _httpClient;
    
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// ctor
        /// </summary>
        public RemoteRestApi(HttpClient httpClient, IHttpContextAccessor httpContextAccessor)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _httpContextAccessor = httpContextAccessor;
        }


        private HttpRequestMessage CreateRequest(HttpMethod method, string uri, HttpContent content)
        {
            const string JsonContentType = "application/json";//Sure TODO??

            var request = new HttpRequestMessage(method, uri);
            try
            {
                request.Headers.Accept.Clear();
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(JsonContentType));
                request.Content = content;
                return request;
            }
            catch
            {
                request.Dispose();
                throw;
            }
        }

        private static async Task<T> DeserializeResponse<T>(HttpResponseMessage response)
        {
            if (RemoteRestApiFactory.ImageContentTypes().Contains(response.Content.Headers.ContentType.MediaType))
            {
                using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                {
                    return (T)Convert.ChangeType(responseStream, typeof(T));
                }
            }
            else 
            {
                throw new NotImplementedException();
            }
        }

        private async Task<TResult> SendAsync<TResult>(HttpMethod method, string uri, HttpContent content, ResponseDeserializer<TResult> responseDeserializer, CancellationToken cancellationToken)
        {
            using (var request = CreateRequest(method, uri, content))
            {
                try
                {
                    var timer = Stopwatch.StartNew();
                    using (var response = await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false))
                    {
                        await EnsureSuccessStatusCode(response);
                        var result = await responseDeserializer(response).ConfigureAwait(false);

                        timer.Stop();
              
                        return result;
                    }
                }
                catch (AggregateException exception) when (exception.InnerExceptions.Count == 1)
                {
                    var innerException = exception.InnerExceptions.Single();

                    if (innerException is OperationCanceledException timeoutException)
                    {
                        var newException = WrapOperationCancelledException(request, timeoutException);
                        throw newException;
                    }
                    else
                    {
                        ExceptionDispatchInfo.Capture(innerException).Throw();
                        throw innerException; // Be calm (успокоился)
                    }
                }
                catch (OperationCanceledException exception)
                {
                    var newException = WrapOperationCancelledException(request, exception);
                    throw newException;
                }
                catch 
                {
                    throw;
                }
            }
        }

        private async Task EnsureSuccessStatusCode(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                return;
            }

            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var errorMessage = $"({(int)response.StatusCode}) {response.StatusCode}, reason:'{response.ReasonPhrase}', content: '{content}'";
            throw new HttpRequestException(errorMessage);
        }

        private static Exception WrapOperationCancelledException(HttpRequestMessage request, OperationCanceledException exception) =>
            new HttpRequestException($"Timeout while requesting REST API ({request.Method} '{request.RequestUri}')", exception);
          
        private static void WaitTask(Task task) =>
            task.GetAwaiter().GetResult();

        private static T WaitTask<T>(Task<T> task) =>
            task.GetAwaiter().GetResult();

#region GET

        public TResult Get<TResult>(string relativeUri, CancellationToken cancellationToken = default) =>
            WaitTask(GetAsync<TResult>(relativeUri, cancellationToken));

        public Task<TResult> GetAsync<TResult>(string relativeUri, CancellationToken cancellationToken = default) =>
            SendAsync<TResult>(HttpMethod.Get, relativeUri, null, DeserializeResponse<TResult>, cancellationToken);

#endregion

    }
}
