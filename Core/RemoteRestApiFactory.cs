using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace AustralianCats.DataAccessRemote
{
    /// <summary>
    /// Управляет созданием экземпляров <see cref="IRemoteRestApi" />
    /// </summary>
    public sealed class RemoteRestApiFactory : IDisposable
    {
        private readonly object _httpClientsSync = new object();
        private readonly Dictionary<string, HttpClient> _httpClients = new Dictionary<string, HttpClient>();

        private readonly IHttpContextAccessor _httpContextAccessor;
     
        /// <summary>
        /// ctor
        /// </summary>
        public RemoteRestApiFactory(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        private HttpClient CreateHttpClient(string baseUri, TimeSpan? timeout)
        {
            var messageHandler = new SocketsHttpHandler
            {
                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip,
            };
            var httpClient = new HttpClient(messageHandler, disposeHandler: true)
            {
                BaseAddress = new Uri(baseUri, UriKind.Absolute)
            };
            if (timeout != null)
            {
                httpClient.Timeout = timeout.Value;
            }

            httpClient.DefaultRequestHeaders.ConnectionClose = false;
            
            return httpClient;
        }

        private HttpClient GetHttpClient(string baseUri, TimeSpan? timeout)
        {
            if (!_httpClients.TryGetValue(baseUri, out var httpClient))
            {
                lock (_httpClientsSync)
                {
                    if (!_httpClients.TryGetValue(baseUri, out httpClient))
                    {
                        httpClient = CreateHttpClient(baseUri, timeout);
                        _httpClients.Add(baseUri, httpClient);
                    }
                }
            }

            return httpClient;
        }

        /// <summary>
        /// Создает <see cref="IRemoteRestApi" /> для доступа к удаленному RESTful сервису с указанным uri
        /// </summary>
        public IRemoteRestApi Create(string url)
        {
            System.TimeSpan requestTimeoutSec = new TimeSpan(0,0,60);

            return new RemoteRestApi(
                GetHttpClient(url, requestTimeoutSec),
                _httpContextAccessor);
        }

        /// <summary>
        /// <see cref="IDisposable" />
        /// </summary>
        public void Dispose()
        {
            lock (_httpClientsSync)
            {
                foreach(var httpClient in _httpClients.Values)
                {
                    httpClient.Dispose();
                }
            }
        }

        public static List<string> ImageContentTypes()
        {
            return new List<string>
            {
               System.Net.Mime.MediaTypeNames.Image.Gif,
               System.Net.Mime.MediaTypeNames.Image.Jpeg,
               System.Net.Mime.MediaTypeNames.Image.Tiff,
               "image/png" // TODO
            };
        }
    }
}