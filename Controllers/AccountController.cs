﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;


namespace AustralianCats
{
    [Authorize(AuthenticationSchemes = "Identity.Application")]
    public class AccountController : Controller
    {
        readonly UserManager<ApplicationUser> userManager;
        readonly SignInManager<ApplicationUser> signInManager;
        private IServiceProvider provider;

   
        public AccountController(
                UserManager<ApplicationUser> userManager,
                SignInManager<ApplicationUser> signInManager,
                ILoggerFactory _loggerFactory,
                IServiceProvider provider
                )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.provider = provider;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var user = await userManager.FindByEmailAsync(model.Email);
                    var token = TokenProviderOptions.GenerateToken(user);
                    TempData["token"] = token;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var user = await userManager.FindByEmailAsync(this.User.Identity.Name);
            if (user == null)
            {// fix cookies still not expired but user not in the store
                return RedirectToAction("Login");
            }
            
            var model = new AccountModel
            {
                Token = TempData["token"] != null ? TempData["token"].ToString() : await userManager.GetAuthenticationTokenAsync(user, TokenAuthOption.TokenType, "Token"),
                CatID = null,
                UserName = this.User.Identity.IsAuthenticated ? this.User.Identity.Name : "Unknown"
            };
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent: false);
                    var token = TokenProviderOptions.GenerateToken(user);
                    TempData["token"] = token;

                    await userManager.SetAuthenticationTokenAsync(user, TokenAuthOption.TokenType, "Token", token);
                    return RedirectToAction("Index");
                }
                AddErrors(result);
            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> GetUserInformation(string email)
        {
            if (string.IsNullOrEmpty(email)) return BadRequest("No email");
            var result = await userManager.FindByEmailAsync(email);
            if (result == null) return BadRequest("User with email '"+ email + "' not found in the store");
            return Ok(result);
        }

        void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public async Task<IActionResult> LogOut()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index");
        }

        #region  check API
        [HttpGet]
        [AllowAnonymous]
        public IActionResult CheckAPI()
        {
            return View();
        }
        #endregion check API
    }
}

