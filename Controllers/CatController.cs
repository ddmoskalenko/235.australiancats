﻿using System;
using System.IO;
using System.Net.Mime;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using AustralianCats.Services;


namespace AustralianCats
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = AuthSchemes)]
    public class CatController : ControllerBase
    {
        private const string AuthSchemes = "Identity.Application" + "," + JwtBearerDefaults.AuthenticationScheme;

        private ICat CatsProvider { get; }
        public CatController(ICat catProvider)
        {
            CatsProvider = catProvider;
        }

        /// <summary>
        /// Get ussual cat, route: /api/cat/eu
        /// </summary>
        [HttpGet("eu")]
        public IActionResult GetEUCat(string mediaType = MediaTypeNames.Image.Jpeg)
        {
            MemoryStream dollyCat = new MemoryStream(CatsProvider.GetRandom().ToArray());
            return CatProto(dollyCat, mediaType);
        }

        /// <summary>
        /// Get turned cat (australian version), route: /api/cat/au
        /// </summary>
        [HttpGet("au")]
        public IActionResult GetAUCat(string mediaType = MediaTypeNames.Image.Jpeg)
        {
            MemoryStream dollyCat = new MemoryStream(CatsProvider.GetRandomTurned(mediaType).ToArray());
            return CatProto(dollyCat, mediaType);
        }

        private IActionResult CatProto(MemoryStream memoryStream, string mediaType)
        {
            if (memoryStream == null || memoryStream.Length == 0) return NoContent();
            try
            {
                memoryStream.Seek(0, SeekOrigin.Begin);
                memoryStream.Position = 0;
                return base.File(memoryStream, mediaType);
            }
            catch (Exception exc)
            {
                return BadRequest(exc);
            }
        }
    }
}
