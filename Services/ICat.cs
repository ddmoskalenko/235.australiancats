﻿using System;
using System.IO;
using System.Drawing;

namespace AustralianCats.Services
{
    public interface ICat
    {
        MemoryStream GetRandom();
        MemoryStream GetRandomTurned(string mediaType);
    }
}
