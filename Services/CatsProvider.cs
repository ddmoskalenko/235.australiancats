﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.Extensions.Logging;
using AustralianCats.DataAccessRemote;


namespace AustralianCats.Services
{
    /// <summary>
    /// Supply communication with service cataas.com 
    /// </summary>
    public class CatsProvider : ICat
    {
        const string site = @"https://cataas.com/";
        const string localUrl = "/cat";

        private readonly ILogger<CatsProvider> _logger;
        private readonly IRemoteRestApi _restAPI;


        public CatsProvider(ILogger<CatsProvider> logger, RemoteRestApiFactory remoteRestFactory) 
        {

            _logger = logger;
            _restAPI = remoteRestFactory.Create(site);
        }

        /// <summary>
        /// Usual picture of Cat 
        /// </summary>
        /// <returns></returns>
        public MemoryStream GetRandom()
        {
            try
            {
                return _restAPI.Get<MemoryStream>(localUrl);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Operation failed");
                return null;
            }
        }

        /// <summary>
        /// Upside-down picture of Cat 
        /// </summary>
        /// <returns></returns>
        public MemoryStream GetRandomTurned(string mediaType)
        {
            try
            {
                var memory  = _restAPI.Get<MemoryStream>(localUrl);
                // trick to make alive stream
                MemoryStream dollyCat = new MemoryStream(memory.ToArray());
                
                var bitmap = new Bitmap(dollyCat);
                MemoryStream resultMemoryStream = new MemoryStream();
                if (bitmap != null)
                {
                    bitmap.RotateFlip(RotateFlipType.Rotate180FlipX);
                    // here is format doesn't have influence on result stream to browser
                    bitmap.Save(resultMemoryStream, ImageFormat.Jpeg);
                }
                else 
                {
                    _logger.LogWarning("Stream is empty");
                }
                return resultMemoryStream;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Operation failed");
                return null;
            }
        }
    }
}

