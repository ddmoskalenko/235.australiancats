
using Microsoft.AspNetCore.Identity;

namespace AustralianCats
{
    /// <summary>
    /// Creating test user 
    /// </summary>
    public class UserCreator
    {
        private UserManager<ApplicationUser> _userManager;

        public UserCreator(UserManager<ApplicationUser> userManager
            )
        {
            _userManager = userManager;
            CreateUsers();
        }

        private async void CreateUsers()
        {
            string email = "a@a.ru";
            string password = "a";

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                user = new ApplicationUser { UserName = email, Email = email };
                var result = await _userManager.CreateAsync(user, password);
            };
            var token = TokenProviderOptions.GenerateToken(user);
            await _userManager.SetAuthenticationTokenAsync(user, TokenAuthOption.TokenType, "Token", token);
            return;
        }
    }
}     