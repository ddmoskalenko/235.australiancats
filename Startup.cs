using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using AustralianCats.Services;
using AustralianCats.DataAccessRemote;


namespace AustralianCats
{

    public class Startup
    {
        public IWebHostEnvironment Enviroment { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            #region bearer
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy(TokenAuthOption.TokenType, new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
                    .RequireAuthenticatedUser().Build());
            });
            #endregion bearer

            services.AddDbContext<ApplicationDbContext>(config =>
            {
                // for in memory database  
                config.UseInMemoryDatabase("MemoryBaseDataBase");
            });

            services.AddIdentity<ApplicationUser, IdentityRole>(config =>
            {
                config.Password.RequiredLength = 1;
                config.Password.RequireDigit = false;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireUppercase = false;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            #region Authentication

            SwaggerConf.Add(services);

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Account/Login/";
                    options.AccessDeniedPath = "/Account/Login/";
                })

               .AddJwtBearer(options =>
               {
                   // use our own custom token validator!
                   options.SecurityTokenValidators.Clear();
                   options.SecurityTokenValidators.Add(new CustomJwtSecurityTokenHandler());

                   options.SaveToken = true;
                   options.RequireHttpsMetadata = false;

                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       IssuerSigningKey = TokenAuthOption.Key,
                       ValidAudience = TokenAuthOption.Audience,
                       ValidIssuer = TokenAuthOption.Issuer,
                       ValidateIssuer = false,
                       ValidateAudience = false,
                       ValidateIssuerSigningKey = false,
                       ValidateLifetime = false,
                       ClockSkew = System.TimeSpan.FromMinutes(0),
                       SaveSigninToken = true
                   };
               });

            #endregion

            services.AddSingleton(provider => Configuration);

            services.AddScoped<RemoteRestApiFactory>();

            services.AddScoped<ICat, CatsProvider>();
            services.AddScoped<UserCreator>();

            services.AddMvc();
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //test user is added
                var scopeeee = app.ApplicationServices.CreateScope();
                scopeeee.ServiceProvider.GetRequiredService<UserCreator>();
            }
            else
            {
                app.UseExceptionHandler("/Account/Error");
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            SwaggerConf.Use(app);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Account}/{action=Index}/{id?}");
            });
        }

        private static class SwaggerConf
        {
            public static void Add(IServiceCollection services)
            {
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "australian-cats", Version = "v1" });
                });
            }

            public static void Use(IApplicationBuilder app)
            {
                app.UseSwagger();

                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("v1/swagger.json", "australian-cats V1");
                });
            }
        }
    }
}
