﻿using System;
using System.Text;
using Microsoft.IdentityModel.Tokens;


namespace AustralianCats
{
    public class TokenAuthOption
    {
        private static string secretKey = "AnFjLk3nseVredd6sEds8wktkemz";
        public static string Audience { get; } = "MyAudience";
        public static string Issuer { get; } = "MyIssuer";
        public static SymmetricSecurityKey Key { get; } = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

        public static SigningCredentials SigningCredentials = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256);
        public static TimeSpan ExpiresSpan { get; } = TimeSpan.FromMinutes(40);
        public static string TokenType { get; } = "Bearer";
    }
}
